/*  "bubbledetect.ino"
    
    An Arduino (UNO) program intended for measuring bubble-activities in
    an airlock of a running fermentation vessel trough use of a photoresitor.
*/

/*
    Copyright (C) 2020  DuckHuntPr0 <Proj. Maling Tørk - dlive.tv/MalingToerk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define SERIAL_BAUDRATE 19200 //no need for more speed i think
#define ANALOG_READ_PIN A0 //where the photoresistor value is read from
#define DELAY_OF_MEASURES_TAKEN 250 //initial ms to wait between each read off of ANALOG_READ_PIN
#define DETECT_PARAMETER 2.0 // pos. or neg. change beyond this value might indicate a bubble gone trough
#define UIDELAY = delay(1)

bool IDLE = true;		// doing fuckall whenever this is set to True
int TOP_VALUE = -9999.0;	// highest A0 value recorded
int BOTTOM_VALUE = 9999.0;	// lowest A0 value recorded
int PREV_VALUE = 0.0;		// previous A0 value
int DIRECTION = 0.0;		// change from PREV_VALUE

void setup()
{
	Serial.begin(SERIAL_BAUDRATE);
	while (!Serial)
	{
		;	/* wait for serial port to connect. Needed for native USB
			(https://www.arduino.cc/reference/en/language/functions/communication/serial/ifserial)
			*/
	}
	delay(100);	
	Serial.println("\033[1;34m.-------------------------------------------------.");
	Serial.println("|                                                 |");
	Serial.println("|              \033[0;32mHELLO MOTHERFØKKER..\033[1;34m               |");
	Serial.println("|                                                 |");
	Serial.println("`----------------------[ dlive.tv/MalingToerk ]---'\033[0m\n");
}



void loop()
{
	// await START command from python script
	if (IDLE)
	{
		Serial.println("\033[32mBREW-MONITOR IN IDLE MODE. AWAITING YER COMMAND SIRE..\033[0m");
	}
	while (IDLE)
	{
		;
	}
		
}
/*
  int value = analogRead(ANALOG_READ_PIN);

  if(value < BOTTOM_VALUE)
  {
    BOTTOM_VALUE = value;
  }

  if(value > TOP_VALUE)
  {
    TOP_VALUE = value;
  }
  
  DIRECTION = PREV_VALUE - value;

  Serial.print(BOTTOM_VALUE);
  Serial.print(",");
  Serial.print(TOP_VALUE);
  Serial.print(",");  
  Serial.print(value);
  Serial.print(",");
  Serial.print(PREV_VALUE);
  Serial.print(",");
  Serial.print(DIRECTION);
  Serial.print("\n");

  
  
  PREV_VALUE = value;
*/
