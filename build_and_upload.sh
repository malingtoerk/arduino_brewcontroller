#!/bin/sh

#see https://arduino.github.io/arduino-cli/getting-started/#compile-and-upload-the-sketch for closer details

### configure according to needs
ARDUINO_DEVICE_SERIAL="/dev/ttyACM0"
ARDUINO_DEVICE_MODEL="arduino:avr:uno"
ARDUINO_SCETCH_FOLDER="brewmonitor"
##

echo "Compiling '$ARDUINO_SCETCH_FOLDER'.."
arduino-cli compile --fqbn $ARDUINO_DEVICE_MODEL $ARDUINO_SCETCH_FOLDER
echo "Uploading '$ARDUINO_SCETCH_FOLDER' to the '$ARDUINO_DEVICE_MODEL' at port '$ARDUINO_DEVICE_SERIAL' .."
arduino-cli upload -p $ARDUINO_DEVICE_SERIAL --fqbn $ARDUINO_DEVICE_MODEL $ARDUINO_SCETCH_FOLDER
